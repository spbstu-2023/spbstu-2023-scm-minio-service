package main

import (
	"context"
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/spbstu-2023/spbstu-2023-scm-minio-service/gen/proto/minio_service/v1"
	"gitlab.com/spbstu-2023/spbstu-2023-scm-minio-service/internal/dump"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

var psqlTarget = &dump.PsqlTarget{
	Host:     "postgres:5432",
	Username: "pguser",
	Password: "pgpwd",
	Database: "testdb",
}

const grpcHost string = "minio-server:8080"

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	l, err := zap.NewProduction()
	if err != nil {
		panic(l)
	}

	l.Info("try to get connection to the psql database")
	psqlConn, err := connToPsql(psqlTarget)
	if err != nil {
		l.Fatal("can't connect to psql", zap.Error(err))
	}
	defer psqlConn.Close()
	l.Info("connect to psql database successful")

	l.Info("try to get connection to the grpc server")
	grpcConn, grpcClient, err := connToGrpc(ctx, grpcHost)
	if err != nil {
		l.Fatal("can't connect to grpc", zap.Error(err))
	}
	defer grpcConn.Close()

	if err := initPsqlData(psqlConn); err != nil {
		l.Fatal("error while init psql", zap.Error(err))
	}
	initialDataInTable, err := getPsqlData(psqlConn)
	if err := initPsqlData(psqlConn); err != nil {
		l.Fatal("error while get init psql", zap.Error(err))
	}

	backupName, err := makeBackup(ctx, grpcClient)
	if err := initPsqlData(psqlConn); err != nil {
		l.Fatal("error while make backup", zap.Error(err))
	}

	if err := updatePsqlData(psqlConn); err != nil {
		l.Fatal("error while update psql data", zap.Error(err))
	}

	updateDataInTable, err := getPsqlData(psqlConn)
	if err != nil {
		l.Fatal("error while update data in psql", zap.Error(err))
	}

	if len(initialDataInTable) == len(updateDataInTable) {
		l.Fatal("input and update data equal", zap.Error(err))
	}

	if err := makeRestore(ctx, grpcClient, backupName); err != nil {
		l.Fatal("error while make restore", zap.Error(err))
	}

	restoredDataInTable, err := getPsqlData(psqlConn)
	if err != nil {
		l.Fatal("error while get reestored data in psql", zap.Error(err))
	}

	if len(initialDataInTable) == len(restoredDataInTable) {
		l.Fatal("input and restored data not equal", zap.Error(err))
	}
}

func connToPsql(target *dump.PsqlTarget) (*sqlx.DB, error) {
	source := fmt.Sprintf("postgresql://%s:%s@%s/%s?sslmode=disable", target.Username, target.Password, target.Host, target.Database)
	db, err := sqlx.Connect("postgres", source)
	if err != nil {
		return nil, err
	}
	return db, nil
}

func initPsqlData(conn *sqlx.DB) error {
	if _, err := conn.Exec("create table if not exists test_table (some_data varchar(255));"); err != nil {
		return err
	}
	if _, err := conn.Exec("insert into test_table(some_data) values ('init_test1'), ('init_test2'), ('init_test3');"); err != nil {
		return err
	}
	return nil
}

func updatePsqlData(conn *sqlx.DB) error {
	if _, err := conn.Exec("insert into test_table(some_data) values ('new_test1'), ('new_test2'), ('new_test3');"); err != nil {
		return err
	}
	return nil
}

func getPsqlData(conn *sqlx.DB) ([]string, error) {
	var results []string
	if err := conn.Select(&results, "select some_data from test_table"); err != nil {
		return nil, err
	}
	return results, nil
}

func connToGrpc(ctx context.Context, host string) (*grpc.ClientConn, minio_service.MinioServiceClient, error) {
	conn, err := grpc.DialContext(ctx, host, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, nil, err
	}
	client := minio_service.NewMinioServiceClient(conn)
	return conn, client, nil
}

func makeRestore(ctx context.Context, c minio_service.MinioServiceClient, name string) error {
	_, err := c.MinioServiceRestore(ctx, &minio_service.MinioServiceRestoreRequest{Name: name})
	if err != nil {
		return err
	}
	return nil
}

func makeBackup(ctx context.Context, c minio_service.MinioServiceClient) (string, error) {
	resp, err := c.MinioServiceBackup(ctx, &minio_service.MinioServiceBackupRequest{})
	if err != nil {
		return "", err
	}
	return resp.Name, nil
}
