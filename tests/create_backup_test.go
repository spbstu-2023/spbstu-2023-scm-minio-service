//go:build integration
// +build integration

package tests

import (
	"context"
	"github.com/stretchr/testify/require"
	tc "github.com/testcontainers/testcontainers-go/modules/compose"
	"sync"
	"testing"
	"time"
)

var dockerComposePaths = []string{
	"testdata/docker/integration.minio.docker-compose.yaml",
	"testdata/docker/integration.minio-server.docker-compose.yaml",
	"testdata/docker/integration.postgres.docker-compose.yaml",
}

const pathToConfig = "testdata/docker/integration-service-config.yaml"
const postgresPort = 5432
const grpcPort = 8080

func TestIntegrationCreateBackup(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	t.Cleanup(cancel)

	t.Log("prepare compose", dockerComposePaths)
	compose, err := tc.NewDockerCompose(dockerComposePaths...)
	require.NoError(t, err, "error while prepare compose: %w")
	t.Log("docker compose created ", dockerComposePaths)

	t.Cleanup(func() {
		t.Log("tru docker compose down ", dockerComposePaths)
		require.NoError(t, compose.Down(ctx, tc.RemoveOrphans(true), tc.RemoveImagesLocal), "can't dont docker-compose")
	})

	t.Log("true docker compose up ", dockerComposePaths)
	require.NoError(t, compose.Up(ctx), "can't start docker-compose")

	runner, err := compose.ServiceContainer(ctx, "integration-runner")
	require.NoError(t, err)

	wg := sync.WaitGroup{}
	wg.Add(1)
	go func() {
		defer wg.Done()
		for {
			state, err := runner.State(ctx)
			require.NoError(t, err)
			t.Log("current service status and log", state.Status, state.ExitCode)
			if state.Status == "exited" {
				require.Equal(t, state.ExitCode, 0, "container down")
				break
			}
			time.Sleep(1 * time.Second)
		}
	}()
	wg.Wait()
}
