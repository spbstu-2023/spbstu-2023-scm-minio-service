package main

import (
	"context"
	"flag"
	"gitlab.com/spbstu-2023/spbstu-2023-scm-minio-service/internal/app/server"
	"gitlab.com/spbstu-2023/spbstu-2023-scm-minio-service/internal/config"
	"gitlab.com/spbstu-2023/spbstu-2023-scm-minio-service/internal/logger"
	"go.uber.org/zap"
	"os"
	"os/signal"
)

func main() {
	pathToConfig := flag.String("config", "configs/config.yaml", "path to config")

	flag.Parse()

	l, err := logger.NewLogger()
	if err != nil {
		panic(err)
	}
	defer logger.CloseLogger(l)

	appConfig, err := config.ParseConfigFromYaml(*pathToConfig)
	if err != nil {
		l.Fatal("error while parse YAML config", zap.Error(err))
	}

	a, err := server.NewApp(appConfig, l)
	if err != nil {
		l.Fatal("error while create new app", zap.Error(err))
	}

	ctx, _ := signal.NotifyContext(context.Background(), os.Interrupt, os.Kill)

	go func() {
		if err := a.Run(); err != nil {
			l.Fatal("error while work grpc server", zap.Error(err))
		}
	}()
	defer a.Stop()

	<-ctx.Done()
	l.Info("get signal and application stop")

}
