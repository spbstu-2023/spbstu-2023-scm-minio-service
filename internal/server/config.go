package server

type Config struct {
	UseReflection bool `yaml:"use-reflection"`
}
