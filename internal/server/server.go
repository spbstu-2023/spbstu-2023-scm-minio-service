package server

import (
	"net"

	"gitlab.com/spbstu-2023/spbstu-2023-scm-minio-service/gen/proto/minio_service/v1"
	"gitlab.com/spbstu-2023/spbstu-2023-scm-minio-service/internal/transport"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

const host = ":8080"

type Server struct {
	listener   net.Listener
	grpcServer *grpc.Server
	l          *zap.Logger
}

func NewServer(config *Config, l *zap.Logger, handler *transport.Handler) (*Server, error) {
	l.Info("start new net listener", zap.String("host", host))
	listener, err := net.Listen("tcp", host)
	if err != nil {
		return nil, err
	}

	grpcServer := grpc.NewServer()
	minio_service.RegisterMinioServiceServer(grpcServer, handler)

	if config.UseReflection {
		l.Info("use reflection for grpc server", zap.String("host", host))
		reflection.Register(grpcServer)
	}

	return &Server{
		listener:   listener,
		grpcServer: grpcServer,
		l:          l,
	}, nil
}

func (s *Server) Run() error {
	s.l.Info("start grpc server",
		zap.String("network", s.listener.Addr().Network()),
		zap.String("host", s.listener.Addr().String()))
	return s.grpcServer.Serve(s.listener)
}

func (s *Server) Close() {
	s.l.Info("grpc server graceful stop",
		zap.String("network", s.listener.Addr().Network()),
		zap.String("host", s.listener.Addr().String()))
	s.grpcServer.GracefulStop()
}
