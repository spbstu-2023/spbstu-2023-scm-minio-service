package minio

type Config struct {
	AccessKey string `yaml:"access-key"`
	SecretKey string `yaml:"secret-key"`
	Ssl       bool   `yaml:"ssl"`
	Host      string `yaml:"endpoint"`
	Bucket    string `yaml:"bucket"`
}
