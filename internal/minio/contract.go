package minio

import (
	"context"
	"io"
	"os"
)

type IManager interface {
	AddToBucket(filePath string, file *os.File) error
	GetFromBucketByName(ctx context.Context, name string) (io.Reader, error)
}
