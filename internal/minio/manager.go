package minio

import (
	"context"
	"fmt"
	"github.com/minio/minio-go/v7"
	_ "github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	_ "github.com/minio/minio-go/v7/pkg/credentials"
	"io"
	"os"
	"sort"
)

const (
	sh     = "/bin/sh"
	tmpDir = "dump_tmp"
)

type Manager struct {
	client *minio.Client
	bucket string
}

func NewManager(config *Config) (*Manager, error) {
	client, err := minio.New(config.Host, &minio.Options{
		Creds:  credentials.NewStaticV4(config.AccessKey, config.SecretKey, ""),
		Secure: config.Ssl,
	})

	if err != nil {
		return nil, err
	}

	return &Manager{
		client: client,
		bucket: config.Bucket,
	}, nil
}

func (m *Manager) AddToBucket(filePath string, file *os.File) error {
	_, err := m.client.PutObject(context.Background(), m.bucket, filePath, file, -1, minio.PutObjectOptions{})
	if err != nil {
		return err
	}
	return nil
}

func (m *Manager) GetFromBucketByName(ctx context.Context, name string) (io.Reader, error) {
	var (
		objInfo *minio.ObjectInfo
		err     error
	)
	if name == "latest" {
		objInfo, err = m.getLastFromBucket()
		if err != nil {
			return nil, err
		}
	} else {
		objInfo = &minio.ObjectInfo{
			Key: name,
		}
	}

	result, err := m.client.GetObject(ctx, m.bucket, objInfo.Key, minio.GetObjectOptions{})
	if err != nil {
		return nil, fmt.Errorf("failed to get object: %v", err)
	}
	return result, nil
}

func (m *Manager) getLastFromBucket() (*minio.ObjectInfo, error) {
	opts := minio.ListObjectsOptions{
		Recursive: true,
	}

	objects := make([]minio.ObjectInfo, 0)
	for obj := range m.client.ListObjects(context.Background(), m.bucket, opts) {
		if obj.Err != nil {
			return nil, fmt.Errorf("failed to list objects: %v", obj.Err)
		}
		objects = append(objects, obj)
	}

	sort.Slice(objects, func(i, j int) bool {
		return objects[i].LastModified.After(objects[j].LastModified)
	})

	if len(objects) < 1 {
		return nil, fmt.Errorf("dump files not found in bucket")
	}
	return &objects[0], nil
}
