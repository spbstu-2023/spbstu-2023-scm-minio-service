//go:build unit
// +build unit

package dump

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestBackupInvoke(t *testing.T) {
	cases := []struct {
		name     string
		backupSh string
		isError  bool
	}{
		{
			name:     "current sh",
			backupSh: "testdata/test.sh",
			isError:  false,
		}, {
			name:     "runnable not found",
			backupSh: "testdata/not-exist.sh",
			isError:  true,
		},
	}
	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			backupSh = c.backupSh
			name, err := Backup(&PsqlTarget{})
			switch c.isError {
			case true:
				assert.Equal(t, "", name)
				assert.Error(t, err)
			case false:
				assert.NotEqual(t, "", name)
				assert.NoError(t, err)
			}
		})
	}

}
