package dump

import (
	"io"
	"os"
	"path"
)

const (
	sh     = "/bin/sh"
	TmpDir = "dump_tmp"
)

func PrepareDir() error {
	if _, err := os.Stat(TmpDir); err != nil {
		if err := os.Mkdir(TmpDir, 0750); err != nil {
			return err
		}
	}
	return nil
}

func ClearDir() error {
	_, err := os.Stat(TmpDir)
	if err == nil {
		if err := os.RemoveAll(TmpDir); err != nil {
			return err
		}
	}
	return nil
}

func CreatFile(name string, r io.Reader) error {
	file, err := os.Create(path.Join(TmpDir, name))
	if err != nil {
		return err
	}
	if _, err := io.Copy(file, r); err != nil {
		return err
	}
	return nil
}

func OpenFile(name string) (*os.File, error) {
	file, err := os.Open(path.Join(TmpDir, name))
	if err != nil {
		return nil, err
	}
	return file, nil
}
