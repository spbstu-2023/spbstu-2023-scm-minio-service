package dump

import (
	"bytes"
	"errors"
	"log"
	"os/exec"
	"path"
)

var restoreSh = "runnable/restore.sh"

func Restore(target *PsqlTarget, name string) error {
	cmd := exec.Command(sh, restoreSh, target.Host, target.Username, target.Password, target.Database, path.Join(TmpDir, name))
	var outb, errb bytes.Buffer
	cmd.Stdout = &outb
	cmd.Stderr = &errb
	if err := cmd.Run(); err != nil {
		log.Println(errb.String())
		return errors.New(errb.String())
	}
	log.Println(outb.String())
	return nil
}
