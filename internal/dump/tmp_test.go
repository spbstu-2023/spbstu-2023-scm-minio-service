//go:build unit
// +build unit

package dump

import (
	"github.com/stretchr/testify/assert"
	"io"
	"os"
	"strings"
	"testing"
)

func TestPrepareDir(t *testing.T) {
	t.Cleanup(func() {
		if err := os.RemoveAll(TmpDir); err != nil {
			t.Fatal("setup error: %w", err)
		}
	})

	assert.NoError(t, PrepareDir(), "Error preparing directory")

	stat, err := os.Stat(TmpDir)
	assert.NoError(t, err, "Error stating directory")

	assert.Equal(t, true, stat.IsDir(), "TmpDir is not a directory")
}

func TestClearDir(t *testing.T) {
	// Set up a temporary directory
	err := os.Mkdir(TmpDir, 0750)
	if err != nil {
		t.Fatal("setup error: %w", err)
	}

	assert.NoError(t, ClearDir(), "Error clearing directory")

	// Check if the directory is removed
	_, err = os.Stat(TmpDir)
	assert.Equal(t, true, os.IsNotExist(err), "TmpDir still exists after clearing")
}

const mockFileContent = "test file content"

func TestCreateAndOpenFile(t *testing.T) {
	// Set up a temporary directory
	PrepareDir()
	t.Cleanup(func() {
		ClearDir()
	})

	r := strings.NewReader(mockFileContent)
	assert.NoError(t, CreatFile(mockFileName, r), "Error creating file")

	file, err := OpenFile(mockFileName)
	assert.NoError(t, err, "Error opening file")
	t.Cleanup(func() {
		file.Close()
	})

	content, err := io.ReadAll(file)
	assert.NoError(t, err, "Error reading file content")
	assert.Equal(t, mockFileContent, string(content))
}
