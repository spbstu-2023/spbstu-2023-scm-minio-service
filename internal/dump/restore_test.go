//go:build unit
// +build unit

package dump

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

const mockFileName = "mock.dump"

func TestRestoreInvoke(t *testing.T) {
	cases := []struct {
		name      string
		restoreSh string
		isError   bool
	}{
		{
			name:      "current sh",
			restoreSh: "testdata/test.sh",
			isError:   false,
		}, {
			name:      "runnable not found",
			restoreSh: "testdata/not-exist.sh",
			isError:   true,
		},
	}
	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			restoreSh = c.restoreSh
			err := Restore(&PsqlTarget{}, mockFileName)
			switch c.isError {
			case true:
				assert.Error(t, err)
			case false:
				assert.NoError(t, err)
			}
		})
	}

}
