package dump

import (
	"bytes"
	"errors"
	"fmt"
	"log"
	"os/exec"
	"path"
	"time"
)

var backupSh = "runnable/backup.sh"

func Backup(target *PsqlTarget) (string, error) {
	n := time.Now()
	name := fmt.Sprintf("%d%d%d_%d%d_%d.dump", n.Year(), n.Month(), n.Day(), n.Hour(), n.Minute(), n.Unix())

	cmd := exec.Command(sh, backupSh, target.Host, target.Username, target.Password, target.Database, path.Join(TmpDir, name))
	var outb, errb bytes.Buffer
	cmd.Stdout = &outb
	cmd.Stderr = &errb
	if err := cmd.Run(); err != nil {
		log.Println(errb.String())
		return "", errors.New(errb.String())
	}
	log.Println(outb.String())
	return name, nil
}
