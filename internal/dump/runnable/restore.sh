psql --dbname=postgresql://$2:$3@$1/$4 -c "DROP SCHEMA public CASCADE; CREATE SCHEMA public;";
psql --dbname=postgresql://$2:$3@$1/$4 -f $5;