package logger

import "go.uber.org/zap"

func NewLogger() (*zap.Logger, error) {
	l, err := zap.NewProduction()
	if err != nil {
		return nil, err
	}
	return l, nil
}

func CloseLogger(l *zap.Logger) error {
	return l.Sync()
}
