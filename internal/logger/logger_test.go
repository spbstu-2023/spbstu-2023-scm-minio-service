//go:build unit
// +build unit

package logger

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewLogger(t *testing.T) {
	l, err := NewLogger()
	assert.NoError(t, err)
	assert.NotNil(t, l)
}
