package server

import (
	"gitlab.com/spbstu-2023/spbstu-2023-scm-minio-service/internal/config"
	"gitlab.com/spbstu-2023/spbstu-2023-scm-minio-service/internal/minio"
	"gitlab.com/spbstu-2023/spbstu-2023-scm-minio-service/internal/server"
	"gitlab.com/spbstu-2023/spbstu-2023-scm-minio-service/internal/transport"
	"go.uber.org/zap"
)

type App struct {
	grpcServer *server.Server
	minio      *minio.Manager
	l          *zap.Logger
}

func NewApp(config *config.Config, l *zap.Logger) (*App, error) {
	mc, err := minio.NewManager(config.Minio)
	if err != nil {
		l.Error("can't start mc", zap.Error(err))
		return nil, err
	}

	handler := transport.NewHandler(mc, config.Target)

	grpcServer, err := server.NewServer(config.GrpcConfig, l, handler)
	if err != nil {
		l.Error("can't start server", zap.Error(err))
		return nil, err
	}
	return &App{
		grpcServer: grpcServer,
		minio:      mc,
		l:          l,
	}, nil
}

func (a *App) Run() error {
	a.l.Info("start application")
	if err := a.grpcServer.Run(); err != nil {
		return err
	}
	return nil
}

func (a *App) Stop() {
	a.l.Info("stop application")
	a.grpcServer.Close()
}
