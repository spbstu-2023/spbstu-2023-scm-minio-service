package transport

import (
	"context"
	"gitlab.com/spbstu-2023/spbstu-2023-scm-minio-service/gen/proto/minio_service/v1"
	"gitlab.com/spbstu-2023/spbstu-2023-scm-minio-service/internal/dump"
	"gitlab.com/spbstu-2023/spbstu-2023-scm-minio-service/internal/minio"
)

type Handler struct {
	mc     minio.IManager
	target *dump.PsqlTarget
}

func NewHandler(mc minio.IManager, target *dump.PsqlTarget) *Handler {
	return &Handler{
		mc:     mc,
		target: target,
	}
}

func (h *Handler) MinioServiceBackup(ctx context.Context, req *minio_service.MinioServiceBackupRequest) (*minio_service.MinioServiceBackupResponse, error) {
	dump.PrepareDir()
	defer dump.ClearDir()

	name, err := dump.Backup(h.target)
	if err != nil {
		return nil, err
	}

	file, err := dump.OpenFile(name)
	if err != nil {
		return nil, err
	}

	if err := h.mc.AddToBucket(name, file); err != nil {
		return nil, err
	}

	return &minio_service.MinioServiceBackupResponse{
		Name: name,
	}, nil
}

func (h *Handler) MinioServiceRestore(ctx context.Context, req *minio_service.MinioServiceRestoreRequest) (*minio_service.MinioServiceRestoreResponse, error) {
	dump.PrepareDir()
	defer dump.ClearDir()

	file, err := h.mc.GetFromBucketByName(ctx, req.Name)
	if err != nil {
		return nil, err
	}

	if err := dump.CreatFile(req.Name, file); err != nil {
		return nil, err
	}

	if err := dump.Restore(h.target, req.Name); err != nil {
		return nil, err
	}
	return &minio_service.MinioServiceRestoreResponse{}, nil
}
