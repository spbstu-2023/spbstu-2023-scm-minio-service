package config

import (
	"gitlab.com/spbstu-2023/spbstu-2023-scm-minio-service/internal/dump"
	"gitlab.com/spbstu-2023/spbstu-2023-scm-minio-service/internal/minio"
	"gitlab.com/spbstu-2023/spbstu-2023-scm-minio-service/internal/server"
	"gopkg.in/yaml.v3"
	"os"
)

type Config struct {
	GrpcConfig *server.Config   `yaml:"grpc-server"`
	Target     *dump.PsqlTarget `yaml:"target"`
	Minio      *minio.Config    `yaml:"minio"`
}

func ParseConfigFromYaml(pathToConfig string) (*Config, error) {
	file, err := os.Open(pathToConfig)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	config := &Config{}
	if err := yaml.NewDecoder(file).Decode(&config); err != nil {
		return nil, err
	}

	return config, nil
}
