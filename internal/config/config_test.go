//go:build unit
// +build unit

package config

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/spbstu-2023/spbstu-2023-scm-minio-service/internal/dump"
	"gitlab.com/spbstu-2023/spbstu-2023-scm-minio-service/internal/minio"
	"gitlab.com/spbstu-2023/spbstu-2023-scm-minio-service/internal/server"
	"path"
	"testing"
)

var mockConfig = &Config{
	GrpcConfig: &server.Config{
		UseReflection: true,
	},
	Target: &dump.PsqlTarget{
		Host:     "test-host",
		Username: "test-user",
		Password: "test-passwd",
		Database: "test-database",
	},
	Minio: &minio.Config{
		AccessKey: "test-access-key",
		SecretKey: "test-secret-key",
		Ssl:       false,
		Host:      "test-endpoint",
		Bucket:    "test-bucket",
	},
}

const testdataPath = "testdata"

func TestParseConfigFromYaml(t *testing.T) {
	cases := []struct {
		name     string
		path     string
		expected *Config
		isError  bool
	}{
		{
			name:     "good config",
			path:     path.Join(testdataPath, "config.yaml"),
			expected: mockConfig,
			isError:  false,
		},
		{
			name:    "invalid config",
			path:    path.Join(testdataPath, "invalid-config.yaml"),
			isError: true,
		},
		{
			name:    "not exists config",
			path:    path.Join(testdataPath, "not-exists-config.yaml"),
			isError: true,
		},
	}
	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			config, err := ParseConfigFromYaml(c.path)
			switch c.isError {
			case true:
				assert.Nil(t, config)
				assert.Error(t, err)
			case false:
				assert.Equal(t, c.expected, config)
				assert.NoError(t, err)
			}
		})
	}

}
