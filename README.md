# spbstu-2023-scm-minio-service

## About
Name: Реализация GitLab CI/CD для развертывания GRPC сервера с дальнейшим развертыванием в k8s.

Authors: [Andrianov Artemiy](https://gitlab.com/andrianovartemii), [Ann Dutova](https://gitlab.com/Ann_Dutova)

Group: #5140904/30202

## Prerequisite
- [Go 1.21](https://tip.golang.org/doc/go1.21)
- [Docker](https://www.docker.com/)
- [Buf Build for Go](https://buf.build/)

> Optional (for Russian users): Buf Build недоступен в России, для работы с ним требуется воспользования подходом `git submodule`.
> 
### Git Submodules requirements
- https://github.com/googleapis/googleapis

## Server configuration
GRPC сервер стартует на порту `:8080`. Для конфигурации используйте:
 - Docker: `-p <your-port>:8080` флаг.
 - k8s: `NodePort`

## Setup local dind runner for GitLab
Для создания локального раннера в Docker (Docker in Docker) - необходимо:

1. Перейти в раздел `Settings -> CI/CD -> Runners` и выбрать `New project runner`.
> Важно - необходимо сохранить токен, который предоставит GitLab, после закрытия окна получить его повторно будет невозможно.
2. Запустить локальный Docker Runner на хост машине:
```bash
    docker volume create gitlab-runner-config
    docker run -d --name gitlab-runner --restart always \                             
        -v /var/run/docker.sock:/var/run/docker.sock \
        -v gitlab-runner-config:/etc/gitlab-runner \
        gitlab/gitlab-runner:latest
```
3. Запустить регистрацию локального раннера:
```bash
    docker exec -it gitlab-runner gitlab-runner register  
```
4. Optional: В конфигурации раннера (`gitlab-runner-config/config.toml`) изменить конфигурацию раннера
```config.toml
  [runners.docker]
    tls_verify = false
    image = "docker:stable"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock"]
    shm_size = 0
    network_mtu = 0
```
5. Optional (for MacOS only): Настроить `DOCKER_HOST` на `unix` сокет:
```bash
    export DOCKER_HOST=unix:///var/run/docker.sock 
```