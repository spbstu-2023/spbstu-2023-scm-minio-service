.PHONY: deps
deps:
	git submodule init;
	go mod tidy;

.PHONY: gogen
gogen: deps
	go generate ./...

.PHONY: build-server
build-server:
	go build -o bin/server cmd/server/main.go

flags=
mode=unit

.PHONY: test
test:
	go test ${flags} -tags=${mode} ./...

.PHONY: dc-start-dev
dc-start-dev:
	docker-compose \
		-f deployment/docker/minio.docker-compose.yaml \
		-f deployment/docker/postgres.docker-compose.yaml \
		-f deployment/docker/minio-server.docker-compose.yaml \
		--env-file=deployment/docker/.env.dev \
		up --build --force-recreate -d

.PHONY: dc-down-dev
dc-down-dev:
	docker-compose \
		-f deployment/docker/minio.docker-compose.yaml \
		-f deployment/docker/postgres.docker-compose.yaml \
		-f deployment/docker/minio-server.docker-compose.yaml \
	  	--env-file=deployment/docker/.env.dev \
		down

